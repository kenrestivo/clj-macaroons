(ns clj-macaroons.core
  (:require [cheshire.core :as json])
  (:import [com.github.nitram509.jmacaroons
            Macaroon
            MacaroonsBuilder
            MacaroonsVerifier
            GeneralCaveatVerifier
            CaveatPacket]
           [com.github.nitram509.jmacaroons.verifier
            TimestampCaveatVerifier]))

(defn add-caveats
  [^MacaroonsBuilder builder caveats]
  (if (seq caveats)
    (recur (.add_first_party_caveat builder (first caveats)) (rest caveats))
    builder))

(def default-separator "=")

(def expiry-separator "<")

(defn exact-caveat
  "Returns a caveat designed to be satisfied exactly.
   k and v should both implement Named"
  ([k]
   (name k))
  ([k v]
   (exact-caveat k v default-separator))
  ([k v separator]
   (str (name k) " " separator " " (name v))))

(defn json-caveat
  "Returns an exact caveat representing multiple k/v pairs, encoded as json."
  [m]
  {:pre [(map? m)]}
  (exact-caveat (json/generate-string m)))

(defn expiry-caveat
  "Returns a caveat that is only be satisfied before expiry when compared
  to machine time.

  Expiry should be one of the formats supported by https://github.com/nitram509/jmacaroons/blob/master/src/main/java/com/github/nitram509/jmacaroons/verifier/TimestampCaveatVerifier.java 
  "
  [expiry]
  (exact-caveat "time" expiry expiry-separator))

(defn satisfy-exact
  [^MacaroonsVerifier mv ^String caveat]
  (.satisfyExcact mv caveat))

(defn satisfy-general
  [^MacaroonsVerifier mv ^GeneralCaveatVerifier gcv]
  (.satisfyGeneral mv gcv))

(defn satisfy-expiry
  [^MacaroonsVerifier mv]
  (satisfy-general mv (TimestampCaveatVerifier.)))

(defmacro verifier->
  [& body]
  `(fn [macaroon# secret-key#]
     (let [^MacaroonsVerifier verifier# (-> (MacaroonsVerifier. macaroon#)
                                            ~@body)]
       (.isValid verifier# secret-key#))))

(defn location
  [^Macaroon macaroon]
  (.-location macaroon))

(defn identifier
  [^Macaroon macaroon]
  (.-identifier macaroon))

(defn signature
  [^Macaroon macaroon]
  (.-signature macaroon))

(defn caveat-packet->str
  [^CaveatPacket caveat-packet]
  (.getValueAsText caveat-packet))

(defn caveats
  [^Macaroon macaroon]
  (map caveat-packet->str (.-caveatPackets macaroon)))

(defn serialize
  [^Macaroon macaroon]
  (.serialize macaroon))

(defn deserialize
  [^String macaroon]
  (MacaroonsBuilder/deserialize macaroon))

(defn macaroon
  [location secret-key identifier & [caveats]]
  (-> (MacaroonsBuilder. location secret-key identifier)
      (add-caveats caveats)
      (.getMacaroon)))

