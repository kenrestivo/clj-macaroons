(defproject clj-macaroons "0.1.1"
  :description "Wrapper for jmacaroons"
  :url "https://gitlab.com/kenrestivo/clj-macaroons"
  :license {:name "MIT"
            :url "https://opensource.org/licenses/MIT"}
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [com.github.nitram509/jmacaroons "0.3.1"]
                 [ cheshire "5.5.0"]])
